import org.junit.Test;
import resources.CalculatorResource;

import javax.ws.rs.NotSupportedException;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        try {
            CalculatorResource calculatorResource = new CalculatorResource();

            String expression = "+100+300";
            assertEquals(400, calculatorResource.calculate(expression));

            expression = " 300 - 99 ";
            assertEquals(201, calculatorResource.calculate(expression));
        }
        catch (NotSupportedException e) {
            System.out.println("Exeption: " + e.getMessage());
        }
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99+1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2-1";
        assertEquals(17, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100*4";
        assertEquals(400, calculatorResource.multiplication(expression));

        expression = "6*6*2";
        assertEquals(72, calculatorResource.multiplication(expression));
    }


    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/2";
        assertEquals(5, calculatorResource.division(expression));

        expression = "36/6/6";
        assertEquals(1, calculatorResource.division(expression));
    }

}
